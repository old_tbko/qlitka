#! /usr/bin/env python

from datetime import (
    datetime,
    timedelta
)
# from pymongo import ReturnDocument

from thread_frame import PollingQThread
from product import Stat
from listen import Listen
import cons as c
import util


class Swipe(PollingQThread):
    def __init__(self, *args, **kwargs):
        PollingQThread.__init__(self, *args, **kwargs)
        self.locks = Stat(self.db).fab()

    def do(self):
        # in 2 incoming collections
        for coll in self.colls:
            coll_name = coll.name
            other_coll_name = [
                el
                for el in [c.MONGO_COLL_A, c.MONGO_COLL_B]
                if el != coll_name
            ][0]

            # find messages with outdated lock
            for doc in coll.find({
                'meta.lock':{
                    '$lt': util.threshold(c.DO_TIMEOUT_SEC) 
                },
                'meta.ttl':{'$exists': False},
            }):
                # and run them again through processing
                self._do_reenter(coll, coll_name, other_coll_name, doc)

        self._remove_periods()

    def _do_reenter(self, coll, coll_name, other_coll_name, doc):
        # check each of 3 possible stuck lock in order
        for lock in self.locks:
            if lock.discover(doc):
                # stuck lock detected, reentering after update

                # define the point of reenter
                doc['meta']['re'] = lock.reenter
                # single run processing thread
                Listen(
                    0, [coll_name, other_coll_name],
                    self.db,
                    0, # run once
                    reenter_doc=doc
                )
                return

        # when no lock found for the doc in stat,
        # reenter it from the very beginning by just removing the lock
        coll.update({'_id': doc['_id']}, {'$unset':{'meta.lock': 1}})

    def _remove_periods(self):
        # remove old periods from stat
        self.db[c.MONGO_COLL_STAT].remove({
            'period': {
                '$lt': util.threshold(
                    c.STAT_PERIOD_SEC * c.STAT_KEEP_PERIODS
                )
            }
        })
