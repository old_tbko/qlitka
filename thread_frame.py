#! /usr/bin/env python

from threading import Thread, Event
import sys
import traceback
import abc
from functools import partial


class PollingQThread(Thread):
    __metaclass__ = abc.ABCMeta

    def __init__(self, coll_names, db, interval):
        self.exc = None
        self.once = interval == 0
        self._should_stop = Event()
        self.db = db
        self.colls = list(map(
            partial(getattr, self.db),
            coll_names
        ))

        self.interval = interval
        Thread.__init__(self, target=self.poll)

    def poll(self):
        try:
            while not self._should_stop.is_set():
                try:
                    self.do()
                except:
                    self.exc = sys.exc_info()
                if self.once:
                    self._should_stop.set()
                self._should_stop.wait(self.interval)
        except Exception as exc:
            print(exc)

    def join(self, *args):
        Thread.join(self, *args)
        if self.exc:
            msg = "Thread '%s' threw an exception: %s %s" % (self.getName(), self.exc[1], self.exc[2])
            new_exc = BaseException(msg)
            traceback.print_tb(self.exc[2])
            raise new_exc

    @abc.abstractmethod
    def do(self):
        pass

    def kill(self):
        try:
            self._should_stop.set()
        except Exception as exc:
            print(exc)
