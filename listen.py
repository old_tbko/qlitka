#! /usr/bin/env python
import re
from datetime import datetime
from pymongo import ReturnDocument
from functools import partial
import copy

from thread_frame import PollingQThread
from product import Stat
import cons as c
import util


# 4 stages:
# 0. not exist
# 1. new and unprocessed = no meta.lock => 2.
# 2. processing = meta.lock:ts => 
#   3.(error | pair with lock) | 4.(no pair found) | 0.(pair found)
# 3. stuck = meta.lock:ts > timeout => 1.(no uid in aggr) | 0.
# 4. unmatched = meta.ttl:sec => 0.(pair found) | 0.(ttl expired)
class Listen(PollingQThread):
    def __init__(self, coll_idx, *args, **kwargs):
        PollingQThread.__init__(self, *args, **kwargs)
        self.coll = self.colls.pop(coll_idx)
        self.pair_coll = self.colls[0]
        self.reenter_doc = kwargs.get('reenter_doc')
        # instances to operate on stat according to different scenarios
        self.statA, self.statAB, self.statB = Stat(self.db).fab()

    def do(self):
        if self.reenter_doc:
            self._reenter()
            return

        doc = self._fetch_new_message()
        if not doc:
            return
        peer = self._find_pair(doc)

        if not peer:
            self._update_single(doc)
        elif peer['meta'].get('ttl'):
            self._update_pair(doc, peer)
        else:
        # match not ready:
        # - do nothing, message will be returned to unprocessed
        # by lock removing after timeout
            pass

    def _reenter(self):
        {
            'X': partial(
                self._after_update_single,
                self.reenter_doc
                ),
            'Y': partial(
                self._after_first_update_pair,
                self.reenter_doc
                ),
            'Z': partial(
                self._after_second_update_pair,
                self.reenter_doc
            ),
        }.get(self.reenter_doc['meta'].get('re'))()

    def _update_single(self, doc):
        # 
        period = util.round_to_periods(doc['meta']['regAt'])
        if not self.statA.update(period, doc, self.coll.name):
            return
        self._after_update_single(doc, period)

    def _after_update_single(self, doc, period=None):
        # (X) =>
        if not period:
            period = util.round_to_periods(doc['meta']['regAt'])
        self._set_ttl_to_single(doc)
        self.statA.unlock(period, doc)

    def _update_pair(self, doc, peer):
        doc = self._merge_with_peer(doc, peer)

        single_period = util.round_to_periods(peer['meta']['regAt'])

        # - decrease unmatched counters for period
        if not self.statAB.update(single_period, peer, self.pair_coll.name):
            return
        self._after_first_update_pair(doc, peer, single_period)

    def refurnish_peer(self, doc):
        peer = copy.deepcopy(doc)
        peer['meta'] = doc['meta']['peer']
        single_period = util.round_to_periods(peer['meta']['regAt'])
        return (peer, single_period)

    def _after_first_update_pair(
        self, doc, peer=None, single_period=None
        ):
        # (Y) =>
        # - increase result counters
        if not peer:
            peer, single_period = self.refurnish_peer(doc)
        pair_period = util.round_to_periods(doc['timestamp'])

        if not self.statB.update(pair_period, doc, ''):
            return
        self._after_second_update_pair(doc, peer, single_period, pair_period)

    def _after_second_update_pair(
        self, doc, peer=None, single_period=None, pair_period=None
        ):
        # (Z) =>
        # - remove source messages
        if not peer:
            peer, single_period = self.refurnish_peer(doc)
        if not pair_period:
            pair_period = util.round_to_periods(doc['timestamp'])

        self.coll.remove({'_id': doc['_id']})
        self.pair_coll.remove({'_id': doc['_id']})
        # - remove call_id lock from period record
        self.statAB.unlock(single_period, peer)
        self.statB.unlock(pair_period, doc)

    def _fetch_new_message(self):
        # read next unprocessed document and put a lock on it
        doc = self.coll.find_one_and_update({
            'meta.lock': {'$exists': False}
        }, {
            '$set': {
                'meta.lock': datetime.utcnow()
            }
        }, return_document=ReturnDocument.AFTER)
        # store registartion timestamp that last till message is removed
        # it designaates the period of unmatched stat
        if doc and not doc['meta'].get('regAt'):
            self.coll.update({
                '_id': doc['_id'],
            }, {
                '$set': {
                    'meta.regAt': doc['meta']['lock'],
                }
            })
            doc['meta']['regAt'] = doc['meta']['lock']

        return doc

    def _find_pair(self, doc):
        # try to found available and processed match
        return self.pair_coll.find_one({
            '_id': doc['_id'],
            'meta.lock': {'$exists': True},
        })

    def _merge_with_peer(self, doc, peer):
        tmp_meta = peer.pop('meta')
        doc.update(peer)
        doc['meta']['peer'] = tmp_meta
        peer['meta'] = tmp_meta
        res = re.search(
            r'\((\d{3})\)', doc['source_number']
        )
        self.coll.update({
            '_id': doc['_id'],
        }, {
            '$set': doc
        })
        doc['code'] = res.groups()[0] if res else 'uknown2'

        return doc

    def _set_ttl_to_single(self, doc):
    # - inject ttl into unmatched message
        self.coll.update({
            '_id': doc['_id'],
        }, {
            '$set': {
                'meta.ttl': datetime.utcnow(),
            },
        })
