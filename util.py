#! /usr/bin/env python
from datetime import (
    datetime,
    timedelta
)
from functools import reduce
import cons as c

def round_to_periods(dt, roundTo=c.STAT_PERIOD_SEC):
    """Round a datetime object to any time laps in seconds
    dt : datetime.datetime object, default now.
    roundTo : Closest number of seconds to round to, default 1 minute.
    Based on:
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
    http://stackoverflow.com/questions/3463930/
    how-to-round-the-minute-of-a-datetime-object-python
    """
    if dt == None : dt = datetime.now()
    seconds = (dt - dt.min).seconds
    rounding = seconds // roundTo * roundTo
    res = dt + timedelta(0,rounding-seconds,-dt.microsecond)
    return res

getpath = lambda doc, path: reduce(lambda d, x: dict.get(d, x, {}), path.split('.'), doc) or None

threshold = lambda seconds_back: datetime.utcnow() - timedelta(seconds=seconds_back)
