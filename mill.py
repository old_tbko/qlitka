#! /usr/bin/env python

import os
import sys
import inspect
from datetime import datetime
import copy

from pymongo import MongoClient

run_path = os.path.realpath(os.path.dirname(
   inspect.getfile(inspect.currentframe())
))
if run_path not in sys.path:
    sys.path.insert(0, run_path)

from speak import MockIncoming
from listen import Listen
from swipe import Swipe
from product import Stat
import cons as c


def create_collections_and_connection():
    client = MongoClient()[c.MONGO_DB_NAME]

    for coll_name in [c.MONGO_COLL_A, c.MONGO_COLL_B]:
        client[coll_name].create_index([('meta.lock', 1)])
        client[coll_name].create_index(
            [('meta.ttl', 1)],
            expireAfterSeconds=c.UNMATCHED_TTL_SEC,
            background=True
        )

    client[c.MONGO_COLL_STAT].create_index([('period', 1)])
    client[c.MONGO_COLL_STAT].create_index([('lock_singles', 1)])
    client[c.MONGO_COLL_STAT].create_index([('lock_singles_pairs', 1)])
    client[c.MONGO_COLL_STAT].create_index([('lock_pairs', 1)])

    return client

def form_polling_list(client):
    subs = []
    subs.append(MockIncoming(
        [c.MONGO_COLL_A, c.MONGO_COLL_B], client, c.POLL_SPEAK_SEC)
    )
    subs.append(
        Listen(0, [c.MONGO_COLL_A, c.MONGO_COLL_B], client, c.POLL_LISTEN_SEC)
    )
    subs.append(
        Listen(1, [c.MONGO_COLL_A, c.MONGO_COLL_B], client, c.POLL_LISTEN_SEC)
    )
    subs.append(
        Swipe([c.MONGO_COLL_A, c.MONGO_COLL_B], client, c.POLL_SWIPE_SEC)
    )
    return subs

if __name__ == '__main__':

    subs = form_polling_list(
        create_collections_and_connection()
    )

    try:
        for sub in subs:
            sub.start()
        while 1:
            for sub in subs:
                sub.join(0.1)
    except KeyboardInterrupt:
        print('wait for termination')
        for sub in subs:
            sub.kill()


class API(object):
    def __init__(self):
        self.client = create_collections_and_connection()
        self.stat = Stat(self.client)

    def get_ttl(self, code):
        def mapper_base(doc):
            res = copy.deepcopy(doc)
            res.pop('_id')
            res['period'] = datetime.strftime(
                res.pop('period'),
                "%Y-%m-%dT%H:%M:%SZ"
            )
            res.pop('lock_singles')
            res.pop('lock_singles_pairs')
            res.pop('lock_pairs')
            return res

        def mapper_ttl(doc):
            doc = mapper_base(doc)
            return doc

        def mapper_code(doc):
            doc = mapper_base(doc)
            val = doc['by_code'].get(code, {})
            print(val)
            doc['by_code'] = {}
            doc['by_code'][code] = val
            return doc

        mapper = mapper_code if code else mapper_ttl
        res = list(map(mapper, self.stat.get()))
        return res
