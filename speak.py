#! /usr/bin/env python

import random
import uuid
from datetime import (
    datetime,
)
import time
from functools import partial

from thread_frame import PollingQThread
from pymongo.errors import DuplicateKeyError


class MockIncoming(PollingQThread):
    def __init__(self, coll_names, db, interval, preset=None):
        self.preset = preset
        self.Sub = (
            None
            if interval == 0
            else
            partial(MockIncoming, coll_names, db, 0)
        )

        PollingQThread.__init__(self, coll_names, db, interval)


    def do(self):
        if self.preset:
            time.sleep(random.randint(50, 5000)/1000)
 
        def random_phone():
            def randnum(magn):
                return '{{:0{}d}}'.format(
                    magn
                ).format(
                    random.randint(0,10 ** magn - 1)
                )
            return '+{}({}){}'.format(
                randnum(1),randnum(3),randnum(7)
            )

        def item_A():
            return {
                'timestamp': datetime.utcnow(),
                'source_number': random_phone(),
                'dest_number': random_phone(),
            }

        def item_B():
            return {
                'duration': random.randint(1,int(1e6))
            }

        colls = self.colls

        selected = random.choice([
            {
                'coll': colls[0],
                'payload': item_A()
            }, {
                'coll': colls[1],
                'payload': item_B()
            }
        ]) if not self.preset else {
            'coll': self.preset['coll'],
            'payload': ({
                'call': item_A,
                'dur': item_B,
            }.get(self.preset['coll'].name))()
        }

        uid = self.preset['uid'] if self.preset else uuid.uuid4()
        selected['payload']['_id'] = uid

        sub = None
        if self.Sub and random.random() > 0.05:
            pair_coll = [el for el in colls if el != selected['coll']][0]
            sub = self.Sub({'uid': uid, 'coll': pair_coll})
            sub.start()

        try:
            selected['coll'].insert(selected['payload'])
        except DuplicateKeyError:
            pass
