#! /usr/bin/env python

import json

from flask import (
	Flask,
	request,
)
app = Flask(__name__)

from mill import API
api = API()


@app.route('/')
def stat():
	return json.dumps(
		api.get_ttl(request.args.get('code'))
	)

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)
