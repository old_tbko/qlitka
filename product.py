#! /usr/bin/env python
from pymongo import ReturnDocument
from functools import partial
import cons as c

from util import (
    getpath,
    round_to_periods,
    threshold,
)

class Stat(object):
    def __init__(self, db, **kw):
        self.db = db

        self.lock_names = None
        if kw:
            self.lock_names = kw['lock_names']
            self.dir = kw['dir']
            self.form_dyn_update = (
                self._form_single_update
                if kw['dir']
                else self._form_pair_update
            )
            self.ts_field = kw['ts_field']
            self.reenter = kw['reenter']

        self.update_rec = {}

    scenarios = (
        (('lock_singles',), 1, 'X', 'meta.regAt'),
        (('lock_singles_pairs', 'lock_singles',), -1, 'Y', 'meta.peer.regAt'),
        (('lock_pairs',), 0, 'Z', 'timestamp'),
    )

    def get(self):
        res = self.db[c.MONGO_COLL_STAT].find({
            'period': {
                '$gt': threshold(
                    c.STAT_PERIOD_SEC * c.STAT_KEEP_PERIODS
                ),
            },
        }).sort('period', -1)
        return res

    def fab(self):
        createStat = partial(Stat, self.db)
        return list(map(
            lambda params: createStat(**params),
            [
                dict(zip(
                    ('lock_names', 'dir', 'reenter', 'ts_field'),
                    el
                ))
                for el in Stat.scenarios
            ]
        ))

    def update(self, period, doc, coll_name):
        if not self.lock_names:
            raise Exception("Use fab method to generate update instances")
        self._init_period_record_if_absent(period)
        self._form_watch(period, doc),
        self._form_update(doc, coll_name),
        return self.db[c.MONGO_COLL_STAT].find_one_and_update(
            self.filter,
            self.update_rec,
            return_document=ReturnDocument.AFTER
        )

    def unlock(self, period, doc):
        self.db[c.MONGO_COLL_STAT].update({
            'period': period,
        }, {
            '$pull': {
                self.lock_names[0]: self.form_doc_lock(doc)
            },
        })

    def discover(self, doc):
        ts = getpath(doc, self.ts_field)
        if not ts:
            return
        period = round_to_periods(ts)
        uid = doc['_id']
        lock_field = self.lock_names[0]

        return self.db[c.MONGO_COLL_STAT].find_one({
            'period': period,
            lock_field + 'uid': uid,
            lock_field + 'ts': {
                '$lt': threshold(
                    c.DO_TIMEOUT_SEC
                ),
            },
        })

    def _form_watch(self, period, doc):
        self.filter = {
            'period': period
        }
        for lock_name in self.lock_names:
            self.filter.update({
                lock_name: {
                    '$not': {
                        '$elemMatch': self.form_doc_lock(doc)
                    }
                }
            })

    def _form_update(self, doc, coll_name):
        self.update_rec.update({
            '$addToSet': {
                self.lock_names[0]: self.form_doc_lock(doc),
            },
        })
        self.form_dyn_update(doc, coll_name)

    def _form_single_update(self, _, coll_name):
        self.update_rec.update({
            '$inc': {
                'miss_{}'.format(coll_name): self.dir,
            },
        })

    def _form_pair_update(self, doc, _):
        code_key = 'by_code.{}.'.format(doc['code'])
        duration = doc['duration']
        self.update_rec.update({
            '$inc': {
                'count': 1,
                'duration': duration,
                code_key + 'count': 1,
                code_key + 'duration': duration,
            },
        })

    def _init_period_record_if_absent(self, period):
        # create empty stat records regarding to current message timestamps
        # if don't exist by now
        self.db[c.MONGO_COLL_STAT].find_one_and_update({
            'period': period,
        }, {
            '$setOnInsert': {
                'period': period,
                'count': 0,
                'duration': 0,
                'by_code': {},
                'miss_call': 0,
                'miss_dur': 0,
                'lock_singles': [],
                'lock_singles_pairs': [],
                'lock_pairs': [],
            }
        }, upsert=True)

    @staticmethod
    def form_doc_lock(doc):
        # print(doc)
        # print(doc['_id'])
        return {
            'uid': str(doc['_id']),
            'ts': doc['meta']['regAt'],
        }
